<?php

namespace Bubbllz\UserBundle\Controller;

use Bubbllz\UserBundle\BubbllzUserBundle;
use Bubbllz\UserBundle\Controller\BaseController;
use Bubbllz\UserBundle\Entity\Groups;
use Bubbllz\UserBundle\Form\GroupsType;
use Bubbllz\UserBundle\Services\GroupService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\Exception;
use Bubbllz\Common\Helpers\Validator;

/**
 * @Route("/")
 */
class GroupController extends BaseController
{

    protected $indexView = "BubbllzUserBundle:Group:Index.html.twig";
    protected $createView = "BubbllzUserBundle:Group:Create.html.twig";

    /**
     * @var GroupService 
     */
    protected $service;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->service = $this->container->get(BubbllzUserBundle::GROUP_SERVICE);
    }

    /**
     * @Route("/group", name="group_index")
     */
    public function indexAction()
    {
        return $this->render('BubbllzUserBundle:Group:index.html.twig');
    }

    /**
     * @Route("/group-create", name="user_group_create")
     */
    public function CreateAction(Request $request)
    {
        $entity = new Groups("", array());
        $form = $this->createForm(GroupsType::class, $entity);
        //$classes = 
        $form->handleRequest($request);
       
        if ($form->isSubmitted() && $form->isValid() && $request->getMethod() == 'POST')
        {  
            try
            {
                if (Validator::IsNullOrEmptyString($entity->getName()))
                {
                    $message = ['error', 'Παρακαλώ συμπληρώστε το όνομά σας'];
                       return $this->redirectToRoute('user_group_create', array());
                }
                $entity->addRole($entity->getAccountType());
                $this->service->Create($entity);
                $message = ['success', 'Απηθκευση'];

                 return $this->redirectToRoute('user_group_create', array());
            } catch (Exception $ex)
            {

                $message = ['error', 'Δεν αποθηκεύτηκε '];
                ;
            }
        };
        return $this->render('BubbllzUserBundle:Groups:Create.html.twig', array(
                    'form' => $form->createView()
        ));
    }

}
