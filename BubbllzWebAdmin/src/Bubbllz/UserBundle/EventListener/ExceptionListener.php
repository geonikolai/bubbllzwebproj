<?php

/**
 * Description of ExceptionListener
 *
 * @author GeorgeG
 */

namespace Bubbllz\UserBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ExceptionListener
{
    /**
     *
     * @var Router
     */
    private $router;
    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    
    public function onException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        //Get the root cause of the exception.
        while (null !== $exception->getPrevious())
        {
            $exception = $exception->getPrevious();
        }
        if ($exception instanceof AccessDeniedException)
        {
            $event->setResponse($this->AccessDeniedException());
        }
    }
    
    private function AccessDeniedException()
    {
        return new RedirectResponse($this->router->generate('fos_user_security_login'));
    }

}
