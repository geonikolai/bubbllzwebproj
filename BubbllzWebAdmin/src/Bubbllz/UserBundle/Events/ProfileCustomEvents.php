<?php

namespace Bubbllz\UserBundle\Events;

/**
 * Description of CategoryEvents
 *
 * 
 */
final class ProfileCustomEvents
{
    const PROFILE_REQUEST_BEFORE_CREATE = "profile.before_create";
    const PROFILE_REQUEST_AFTER_CREATE = "profile.after_create";
}
