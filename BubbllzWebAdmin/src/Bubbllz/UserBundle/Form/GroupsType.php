<?php

namespace Bubbllz\UserBundle\Form;

use Bubbllz\Common\Singletons\AccountType;
use Bubbllz\Common\Singletons\CrudRoles;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupsType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->
                add('name', null, array(
                    'attr' => array(
                        'class' => 'input validate[required], full-width',
                    ),
                    'required' => true,
                        )
                )
                
                    ->add('accountType', ChoiceType::class, [
                    'choice_list' => new ChoiceList(
                            AccountType::getInstance()->getAccountTypes(),
                             AccountType::getInstance()->getAccountTypes()
                    ),
                    'attr' => array(
                        'class' => 'select',
                    ),
                    'empty_value' => 'Επιλογή Τύπου Χρήστη',
                    'multiple' => false,
                    'required' => false,
                    'choices_as_values' => true,
        ])
                
                
        
                ->add('roles', ChoiceType::class, [
                    'choice_list' => new ChoiceList(
                            CrudRoles::getInstance()->getCrudRoles(), 
                            CrudRoles::getInstance()->getCrudRoles()
                    ),
                    'attr' => array(
                        'class' => 'select allow-empty easy-multiple-selection check-list validate[required] ',
                    ),
                    'empty_value' => 'Επιλογή Ρόλου',
                    'multiple' => true,
                    'required' => false,
                    'choices_as_values' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bubbllz\UserBundle\Entity\Groups'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bubbllz_userbundle_groups';
    }

}
