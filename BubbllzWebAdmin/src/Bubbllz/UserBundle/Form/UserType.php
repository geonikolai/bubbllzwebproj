<?php

namespace Bubbllz\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                  ->add('groups', 'entity', array(
                    'class' => 'BubbllzUserBundle:Groups',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                                ->select("partial d.{id,name}")
                                ->orderBy('d.name', 'ASC');
                    },
                    'intention' => 'group',
                    'empty_value' => 'Group Ρόλων',
                    'required' => false,
                    'multiple' => true,
                    'attr' => array(
                        'class' => 'select full-width multiple-as-single easy-multiple-selection check-list validate[required]',
                    )
                ))
                ->add('username', null, [
                    'attr' => [
                         'class' => ' full-width input',
                    ]
                ])
                ->add('email', null, array(
                    'attr' => array(
                        'class' => "input full-width",
                    ),
                    'required' => true,
                        )
                )
                ->add('plainPassword', 'Symfony\Component\Form\Extension\Core\Type\RepeatedType', array(
                    'attr' => array(
                        'class' => "input full-width",
                    ),
                    'required' => true,
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password', 'attr' => ['class' => 'input full-width']),
                    'second_options' => array('label' => 'form.password_confirmation', 'attr' => ['class' => 'input full-width']),
                    'invalid_message' => 'fos_user.password.mismatch',
                        )
                )
        ;
    }
    
      public function getParent() {
        //return 'FOS\UserBundle\Form\Type\RegistrationFormType';
        // Or for Symfony < 2.8
        return 'fos_user_registration';
    }
  /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'account_form_registration';
    }
   

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bubbllz\UserBundle\Entity\User'
        ));
    }
    
    
   public function __toString()
    {
        return $this->username;
    }


  

}
