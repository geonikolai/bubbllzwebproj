<?php

namespace Bubbllz\HomeBundle\Models;



class ContactForm {

    private $name;
    private $email;
    private $message;

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @return string
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

     /**
     * Get name
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @return string
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    
        /**
     * Get name
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set name
     *
     * @return string
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

}
