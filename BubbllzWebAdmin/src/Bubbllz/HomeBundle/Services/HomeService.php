<?php

namespace Bubbllz\HomeBundle\Services;

use Bubbllz\Common\CustomEvents\AuditCustomEvents;
use Bubbllz\Common\CustomEvents\AuditEvent;
use Bubbllz\EntitiesBundle\Entity\TalkClient;
use Bubbllz\Common\Services\AuditService;

/**
 * Description of HomeService
 *
 * 
 */
class HomeService extends AuditService
{

    /**
     *
     * @var TalkClient
     */
    public function Create($entity)
    {



        //Dispatch an event before creating the ENTITY
//        $dispatcher = $this->container->get('event_dispatcher');
//        $event = new AuditEvent($entity);
//        $dispatcher->dispatch(AuditCustomEvents::BEFORE_CREATE, $event);



        parent::Create($entity);
        //Dispatch one more event after creating the product
//        $dispatcher->dispatch(AuditCustomEvents::AFTER_CREATE, $event);
    }

}
