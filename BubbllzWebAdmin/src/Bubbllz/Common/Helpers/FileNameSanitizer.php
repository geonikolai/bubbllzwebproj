<?php

namespace Bubbllz\Common\Helpers;

/**
 * Sanitizes a file name changing it with a predefined mask
 *
 * @author pilixonis
 */
class FileNameSanitizer
{
    public static function sanitaze($filename)
    {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        $name = rand(100000, 999999) . '_' . rand(100000, 999999) . '.' . $ext;
        return $name;
    }
}
