<?php

namespace Bubbllz\Common\Helpers;
class Validator
{

    /**
     * 
     * @param string $string
     * @return bool
     */
    static function IsNullOrEmptyString($string)
    {
        return (!isset($string) || trim($string) === '');
    }

    /**
     * 
     * @param string $strings
     * @return bool
     */
    static function IsNullOrEmptyStrings($strings)
    {
        foreach ($strings as $string)
        {
            return (!isset($string) || trim($string) === '');
        }
    }

}
