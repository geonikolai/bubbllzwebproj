<?php

namespace Bubbllz\Common\Services;


use Bubbllz\Common\Services\Service;
use Bubbllz\EntitiesBundle\Interfaces\IAuditEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Bubbllz\Common\Helpers\Dates;

abstract class AuditService extends Service
{

    public function __construct(ContainerInterface $container, $class)
    {
        parent::__construct($container, $class);
    }

    /**
     * Create an entity to the database and updates its' audit properties
     * @param IAuditEntity $entity
     */
    public function Create($entity)
    {
        //Set auditory properties
        $entity->SetCreatedDate(Dates::GetNow());
        $entity->SetModifiedDate(Dates::GetNow());

        //Call parent create method
        parent::Create($entity);
        
       
       
    }

    /**
     * Updates an entity and updates its' audit properties
     * @param IAuditEntity $entity
     */
    public function Update($entity = null)
    {
        //If entity is sent to the update function set auditory properties
        if ($entity != null)
        {
            //$entity->SetModifiedBy(TokenStorageInterface::getToken()->getUser()->GetId());
            $entity->SetModifiedDate(Dates::GetNow());
        }

        //Call parent method
        parent::Update($entity);
    }

}
