<?php

namespace Bubbllz\Common\CustomEvents;

/**
 * Description of AuditCustomEvents
 *
 * 
 */
final class AuditCustomEvents
{

    const BEFORE_CREATE = "audit.before_create";
    const AFTER_CREATE = "audit.after_create";
    const BEFORE_UPDATE = "audit.before_update";
    const AFTER_UPDATE = "audit.after_update";

}
