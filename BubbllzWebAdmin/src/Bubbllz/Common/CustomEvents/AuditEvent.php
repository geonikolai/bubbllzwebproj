<?php

namespace Bubbllz\Common\CustomEvents;

use Bubbllz\EntitiesBundle\Interfaces\IAuditEntity;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of AuditEvent
 *
 * 
 */
class AuditEvent extends Event
{
    /**
     *
     * @var IAuditEntity auditEntity
     */
    protected $auditEntity;
    
    public function __construct(IAuditEntity &$auditEntity)
    {
        $this->auditEntity = $auditEntity;
    }
    
    /**
     * 
     * @return auditEntity
     */
    public function &getAudit()
    {
        return $this->auditEntity;
    }
}
