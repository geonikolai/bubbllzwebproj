<?php

namespace Bubbllz\KernelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class DashboardController extends Controller
{

    /**
     * @Route("/kernel/admin/dashboard/index", name="kernel_dashboard_index")
     */
    public function indexAction()
    {
        return $this->render('BubbllzKernelBundle:Dashboard:index.html.twig');
    }

}
