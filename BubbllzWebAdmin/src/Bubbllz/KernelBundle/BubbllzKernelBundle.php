<?php

namespace Bubbllz\KernelBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


class BubbllzKernelBundle extends Bundle
{
    const ACCOUNT_SERVICE =  "kernelbundle_account_service";
}
