<?php

namespace Bubbllz\CompanyBundle\Entity;

use Bubbllz\AppKernelBundle\Models\IAuditEntity;
use Bubbllz\AppKernelBundle\Models\IEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 *
 * @ORM\Entity
 * @ORM\Table(name="bubbllz_companies")
 */
class Company implements IEntity, IAuditEntity
{

    use \Bubbllz\AppKernelBundle\Traits\EntityTrait;

    use \Bubbllz\AppKernelBundle\Traits\AuditTrait;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="brand_name", type="string", nullable=false)
     */
    private $brandName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="official_name", type="string", nullable=false)
     */
    private $officialName;

    /**
     * @var integer
     *
     * @ORM\Column(name="afm", type="string", nullable=true)
     */
    private $afm;

    /**
     * @var string
     *
     * @ORM\Column(name="doy", type="string", length=100, nullable=true)
     */
    private $doy;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=20, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=60, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address_no", type="string", length=60, nullable=true)
     */
    private $addressNo;
    
     /**
     * 
     * @ORM\ManyToMany(targetEntity="\Bubbllz\UserBundle\Entity\User")
     * @ORM\JoinTable(name="bubbllz_company_users",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $users;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set brandName
     *
     * @param string $brandName
     *
     * @return Company
     */
    public function setBrandName($brandName)
    {
        $this->brandName = $brandName;

        return $this;
    }

    /**
     * Get brandName
     *
     * @return string
     */
    public function getBrandName()
    {
        return $this->brandName;
    }

    /**
     * Set officialName
     *
     * @param string $officialName
     *
     * @return Company
     */
    public function setOfficialName($officialName)
    {
        $this->officialName = $officialName;

        return $this;
    }

    /**
     * Get officialName
     *
     * @return string
     */
    public function getOfficialName()
    {
        return $this->officialName;
    }

    /**
     * Set afm
     *
     * @param string $afm
     *
     * @return Company
     */
    public function setAfm($afm)
    {
        $this->afm = $afm;

        return $this;
    }

    /**
     * Get afm
     *
     * @return string
     */
    public function getAfm()
    {
        return $this->afm;
    }

    /**
     * Set doy
     *
     * @param string $doy
     *
     * @return Company
     */
    public function setDoy($doy)
    {
        $this->doy = $doy;

        return $this;
    }

    /**
     * Get doy
     *
     * @return string
     */
    public function getDoy()
    {
        return $this->doy;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Company
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Company
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set addressNo
     *
     * @param string $addressNo
     *
     * @return Company
     */
    public function setAddressNo($addressNo)
    {
        $this->addressNo = $addressNo;

        return $this;
    }

    /**
     * Get addressNo
     *
     * @return string
     */
    public function getAddressNo()
    {
        return $this->addressNo;
    }

    /**
     * Add user
     *
     * @param \Bubbllz\UserBundle\Entity\User $user
     *
     * @return Company
     */
    public function addUser(\Bubbllz\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Bubbllz\UserBundle\Entity\User $user
     */
    public function removeUser(\Bubbllz\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
