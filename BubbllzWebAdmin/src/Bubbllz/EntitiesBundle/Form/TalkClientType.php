<?php

namespace Bubbllz\EntitiesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TalkClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
     
  
        $builder
            ->add('fullName', null, array(
                "required" => true
            ))
            ->add('profession', null, array(
                "required" => false
            ))
            ->add('email', null, array(
                "required" => false
            ))
            ->add('contactPhone', null, array(
                "required" => true
            ))
            ->add('city', null, array(
                "required" => false
            ))
        ;
    
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bubbllz\EntitiesBundle\Entity\TalkClient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bubbllz_entitiesbundle_talkclient';
    }


}
