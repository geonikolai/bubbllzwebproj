<?php

namespace Bubbllz\EntitiesBundle\Interfaces;

interface IEntity
{
    public function getId();
}
