<?php

namespace Bubbllz\EntitiesBundle\Interfaces;

interface IFile extends IEntity
{

   
    public function setPath($path);

    public function getPath();
    
    public function setFile(UploadedFile $file);
    
    public function getFile();

    public function getAbsolutePath();

    public function getWebPath();

    public function getUploadRootDir();

    public function getUploadDir();
    
    public function upload($directory = '');
}
