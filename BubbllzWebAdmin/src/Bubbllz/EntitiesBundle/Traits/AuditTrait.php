<?php

namespace Bubbllz\EntitiesBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

use \DateTime;

trait AuditTrait {
  
    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $modifiedDate;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $createdBy;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $modifiedBy;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    protected $deleted;


    /**
     * Set createdDate
     *
     * @param DateTime $createdDate
     *
     * @return /Date
     */
    public function setCreatedDate(DateTime $createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param DateTime $modifiedDate
     *
     * @return  mixed
     */
    public function setModifiedDate(DateTime $modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set createdBy
     *
     * @param  /Bubbllz/UserBundle/Entity/User $createdBy
     *
     * @return /Bubbllz/UserBundle/Entity/User
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return  /Bubbllz/UserBundle/Entity/User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

      /**
     * Set modifiedBy
     *
     * @param string /Bubbllz/UserBundle/Entity/User $modifiedBy
     *
     * @return /Bubbllz/UserBundle/Entity/User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }
    

    /**
     * Get modifiedBy
     *
     * @return /Bubbllz/UserBundle/Entity/User
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }
    
  

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
      /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return  boolean
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }
    
    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
    
      /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return  boolean
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }
    
 
}

