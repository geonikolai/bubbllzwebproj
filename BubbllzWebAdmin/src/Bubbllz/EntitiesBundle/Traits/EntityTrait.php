<?php

namespace Bubbllz\EntitiesBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait EntityTrait {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

