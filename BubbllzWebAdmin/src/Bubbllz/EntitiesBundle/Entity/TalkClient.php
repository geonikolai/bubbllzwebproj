<?php

namespace Bubbllz\EntitiesBundle\Entity;

use Bubbllz\EntitiesBundle\Interfaces\IAuditEntity;
use Bubbllz\EntitiesBundle\Interfaces\IEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TalkClient
 *
 * @ORM\Entity
 * @ORM\Table(name="bubbllz_talk_client")
 */
class TalkClient implements IEntity, IAuditEntity
{

    use \Bubbllz\EntitiesBundle\Traits\EntityTrait;

    use \Bubbllz\EntitiesBundle\Traits\AuditTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="talk_client_name", type="string", length=255, nullable=false)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=255, nullable=true)
     */
    private $profession;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="contactPhone", type="string", length=100, nullable=false)
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;


    

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return TalkClient
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return TalkClient
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return TalkClient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     *
     * @return TalkClient
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return TalkClient
     */
    public function setCity($city) 
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
}
