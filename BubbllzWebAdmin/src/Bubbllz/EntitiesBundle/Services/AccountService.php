<?php

namespace Bubbllz\EntitiesBundle\Services;

use Bubbllz\EntitiesBundle\Entity\Account;
use Bubbllz\Common\Services\AuditService;

/**
 * Description of AccountService
 *
 * 
 */
class AccountService extends AuditService
{

    /**
     *
     * @var Account
     */
    public function Create($entity)
    {
       
        parent::Create($entity);
        
       
    }

    public function CheckEmailExists($user)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        if ($userManager->findUserBy(array('emailCanonical' => $this->canonicalizeEmail($user->getEmail()))) != null)
        {
            return true;
        } else
        {
            return false;
        }
    }

}
