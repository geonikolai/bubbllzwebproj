(function ($) {
    $.gglib.Window = function (options)
    {
        var me = this;
        me.window = undefined;
        //set default options
        me.settings = {
            windowSettings: {
                height: "auto",
                width: "auto",
                maxHeight: 1000,
                maxWidth: 1000,
                theme: 'office',
                isModal: true,
                okButton: $('[type="submit"]'),
                resizable: true
            },
            hasForm: false,
            formSubmitType: 'ajax',
            windowContent: undefined,
            contentUrl: undefined,
            ready: undefined
        };
        //Process user options
        $.extend(me.settings, options);

        me.removeWindow = function () {
            //Wait for the animation to end before entirely remove the window from dom
            setTimeout(function () {
                $(me.window).remove();
            }, 500);

        };

        me.closeWindow = function () {
            $(me.window).jqxWindow('close');
        };

        me.refreshWindowContent = function () {
            $(me.window).remove();
            build();
        };
        
        var WindowReady = function(){
            me.settings.ready(me);
        };

        var InitializeAjaxForm = function () {
            me.window.find("form").submit(function () {
                var $form = $(this);
                var data = new FormData($form);
                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data, textStatus, xhr) {
                        //If status is 200 means we had an error on form post so we reload the new content
                        if (xhr.status == 200) {
                            me.settings.data = data;
                            me.refreshWindowContent();
                        } else if (xhr.status == 302) {
                            var parsedData = parseJSON(data);
                            //302 status means that form is posted without errors so we show a dialog with the success message if is any
                            //Raise an event to inform that post succeeded and pass the data to the event
                            if (parsedData.content !== undefined && parsedData.title !== undefined) {
                                $.gglib.Dialog({title: parsedData.title, content: parsedData.content});
                                me.window.trigger("windowForm:success", [parsedData]);
                            }
                        }
                    }
                });
                return false;
            });
        };

        var RegisterEvents = function () {
            //On dialog close event
            $(me.window).on('close', function () {
                me.removeWindow();
            });

            //On no button click event
            $(me.window).find('.btn-cancel').on('click', function () {
                me.closeWindow();
            });

            //Set modal key strokes
            me.window.keyup(function (event) {
                switch (event.keyCode)
                {
                    //Esc button hit
                    case 27:
                        me.closeWindow();
                        break;

                }
            });

            if (me.settings.hasForm) {
                switch (me.settings.formSubmitType) {
                    case 'ajax':
                        InitializeAjaxForm();
                        break;
                }

            }
        };

        me.open = function () {

            //If window data is not set alert user and prevent window creaion
            if (me.settings.windowContent === undefined && me.settings.contentUrl === undefined) {
                var dialog = $.gglib.Dialog({title: "Warniong", content: "Window content is not set."});
                return false;
            }

            if (me.settings.contentUrl !== undefined) {
                //if content url is set, make an ajax request and then open the window with the response data
                $.ajax({
                    url: me.settings.contentUrl,
                    success: function (data) {
                        me.settings.windowContent = data;
                        me.window = $(data).jqxWindow(me.settings.windowSettings);
                        RegisterEvents();
                        WindowReady();
                    }
                });
            } else {
                //if content url is not set then windowContent must have been set already so load the window
                me.window = $(me.settings.windowContent).jqxWindow(me.settings.windowSettings);
                RegisterEvents();
                WindowReady();
            }


        };

        return me;
    };
}(jQuery));