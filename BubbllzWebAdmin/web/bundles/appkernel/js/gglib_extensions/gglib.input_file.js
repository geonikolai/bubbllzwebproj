(function ($) {
    $.gglib.File = function (options)
    {
        var me = this;

        me.file = undefined;
        me.fileTextClassName = 'gglib-file-text';
        me.fileContainerClassName = 'gglib-file-container';
        me.fileWrapperClassName = 'gglib-file-wrapper';

        //set default options
        me.settings = {
            element: $('.fileinput'),
            buttonColor: 'rgb(92, 184, 92)',
            text: '<i class="fa fa-search"></i>',
            textColor: 'white',
            textWidth: '100%'
        };

        //Process user options
        $.extend(me.settings, options);

        me.Draw = function () {
            //Create the text field element
            var textField = document.createElement("input");
            textField.className = 'form-control ' + me.fileTextClassName;
            textField.style.width = me.settings.textWidth;
            textField.style.float = 'left';
            textField.setAttribute("type", "text");
            textField.setAttribute("readonly", "readonly");

            //Create the container of the file element
            var fileContainer = document.createElement("span");
            fileContainer.className = me.fileContainerClassName;

            //Create the wrapper to inject the input element and the file container
            var wrapper = document.createElement('div');
            wrapper.className = me.fileWrapperClassName;

            //Append the text-field to the wrapper
            $(wrapper).append($(textField));

            //Prepend the file container to the wrapper
            $(wrapper).prepend($(fileContainer));

            //Inject the wrapper exactly before input element
            $(wrapper).insertBefore(me.settings.element);

            $(wrapper).append($('<div style="clear: left"></div>'));

            //Append the file input to its container
            $(fileContainer).append(me.settings.element);
            $(fileContainer).prepend($('<span>' + me.settings.text + '</span>'));

            //Style the field
            $(fileContainer).css({
                background: me.settings.buttonColor,
                cursor: 'pointer',
                '-webkit-border-top-left-radius': '5px',
                '-webkit-border-bottom-left-radius': '5px',
                '-moz-border-radius-topleft': '5px',
                '-moz-border-radius-bottomleft': '5px',
                'border-top-left-radius': '5px',
                'border-bottom-left-radius': '5px',
                position: 'absolute',
                left: '10px',
                overflow: 'hidden',
                padding: '8px 15px',
                color: me.settings.textColor,
                display: 'inline-block',
                float: 'left'
            });

            me.settings.element.css({
                position: 'absolute',
                top: 0,
                right: 0,
                'font-size': '12px',
                'min-width': '100%',
                'min-height': '100%',
                'text-align': 'right',
                filter: 'alpha(opacity=0)',
                'opacity': '0.1',
                'outline': 'none',
                cursor: 'inherit',
                display: 'block'
            });

            $(wrapper).find('[type=text]').css({
                'cursor': 'pointer',
                'padding-left': '45px',
                'margin-left': '-5px',
                '-webkit-border-top-left-radius': '0px!important',
                '-webkit-border-bottom-left-radius': '0px!important',
                '-moz-border-radius-topleft': '0px!important',
                '-moz-border-radius-bottomleft': '0px!important',
                'border-top-left-radius': '0px!important',
                'border-bottom-left-radius': '0px!important'
            });

            me.file = $(wrapper);
        };

        me.Open = function () {
            me.file.find('[type=file]').click();
        };

        var handlers = function () {
            me.file.find('[type=text]').click(function () {
                me.Open();
            });

            me.file.find('[type=file]').change(function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                me.file.find('[type=text]').val(label);
                //me.file.find('span span').text('(' + numFiles + ') Files');
            });
        };

        var build = function () {
            me.Draw();
            handlers();
        };

        build();

        return me;
    }
}(jQuery));