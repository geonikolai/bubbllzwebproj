(function ($) {
    $.gglib.YesNoDialog = function (options)
    {
        var me = this;
        me.window = undefined;
        //set default options
        me.settings = {
            title: 'Are you sure',
            content: 'Perform the action?',
            windowSettings: {
                minHeight: 200, 
                minWidth: 400, 
                isModal: true, 
                theme: 'office',
                autoOpen: true
            }
        };
        //Process user options
        $.extend(me.settings, options);

        me.layout = undefined;
        var getLayout = function () {

            if (me.layout !== undefined) {
                return me.layout;
            }

            //Create the outer element
            var html = document.createElement('div');
            html.className = 'window';

            //Create the header section of the dialog
            var header = document.createElement('div');
            header.className = 'header';
            header.innerHTML = me.settings.title;

            //Create the body section of the dialog
            var body = document.createElement('div');
            body.className = 'body';

            var top = document.createElement('div');
            top.className = 'top';

            var clear = document.createElement('div');
            clear.className = 'clearfix';

            var icon = document.createElement('div');
            icon.className = 'yes-no-dialog-icon';
            var i = document.createElement('i');
            i.className += 'fa fa-warning fa-5x';
            icon.appendChild(i)
            top.appendChild(icon);

            var content = document.createElement('div');
            content.className = 'yes-no-dialog-content';
            content.innerHTML = me.settings.content;
            top.appendChild(content);

            body.appendChild(top);

            var footer = document.createElement('div');
            footer.className = 'footer';

            var buttonYes = document.createElement('button');
            buttonYes.className = 'btn btn-default btn-yes';
            buttonYes.innerHTML = '<i class="fa fa-check"></i> Yes';
            var buttonNo = document.createElement('div');
            buttonNo.className = 'btn btn-default btn-no';
            buttonNo.innerHTML = '<i class="fa fa-times"></i> No';

            footer.appendChild(buttonYes);
            footer.appendChild(buttonNo);

            body.appendChild(footer);

            html.appendChild(header);
            html.appendChild(body);

            me.layout = html;
            return me.layout;
        };

        me.removeWindow = function () {
            //Wait for the animation to end before entirely remove the window from dom
            setTimeout(function () {
                $(me.window).remove();
            }, 500);

        };
        
        me.closeWindow = function(){
            $(me.window).jqxWindow('close');
        };

        var registerEvents = function () {
            //On dialog close event
            $(me.window).on('close', function () {
                me.removeWindow();
            });

            //On no button click event
            $(me.window).find('.btn-no').on('click', function () {
                $(me.window).jqxWindow('close');
            });
            
            //On yes button click event
            $(me.window).find('.btn-yes').on('click', function () {
                $(me.window).trigger('dialog-yes-click');
            });
        };

        var build = function () {
            me.window = $(getLayout()).jqxWindow(me.settings.windowSettings);
            registerEvents();
        };

        build();

        return me;
    };
}(jQuery));