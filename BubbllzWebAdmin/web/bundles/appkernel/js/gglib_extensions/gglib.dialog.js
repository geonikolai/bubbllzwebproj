(function ($) {
    $.gglib.Dialog = function (options)
    {
        var me = this;
        me.window = undefined;
        //set default options
        me.settings = {
            title: 'Are you sure',
            content: 'Perform the action?',
            theme: 'office',
            windowSettings: {
                minHeight: 200, 
                minWidth: 400,  
                isModal: true, 
                theme: 'office',
                autoOpen: true
            }
        };
        
        var onOkClickHandlers = [];
        
        //Process user options
        $.extend(me.settings, options);

        me.layout = undefined;
        var getLayout = function () {

            if (me.layout !== undefined) {
                return me.layout;
            }

            //Create the outer element
            var html = document.createElement('div');
            html.className = 'window';

            //Create the header section of the dialog
            var header = document.createElement('div');
            header.className = 'header';
            header.innerHTML = me.settings.title;

            //Create the body section of the dialog
            var body = document.createElement('div');
            body.className = 'body';

            var top = document.createElement('div');
            top.className = 'top';

            var clear = document.createElement('div');
            clear.className = 'clearfix';

            var icon = document.createElement('div');
            icon.className = 'yes-no-dialog-icon';
            var i = document.createElement('i');
            i.className += 'fa fa-warning fa-5x';
            icon.appendChild(i)
            top.appendChild(icon);

            var content = document.createElement('div');
            content.className = 'yes-no-dialog-content';
            content.innerHTML = me.settings.content;
            top.appendChild(content);

            body.appendChild(top);

            var footer = document.createElement('div');
            footer.className = 'footer';
            
            var buttonOk = document.createElement('div');
            buttonOk.className = 'btn btn-default btn-ok pull-right';
            buttonOk.innerHTML = '<i class="fa fa-check"></i> Ok';

            footer.appendChild(buttonOk);

            body.appendChild(footer);

            html.appendChild(header);
            html.appendChild(body);

            me.layout = html;
            return me.layout;
        };

        me.removeWindow = function () {
            //Wait for the animation to end before entirely remove the window from dom
            setTimeout(function () {
                $(me.window).remove();
            }, 500);

        };
        
        me.closeWindow = function(callback){
            $(me.window).jqxWindow('close');
            
            if(callback !== undefined){
                callback();
            }
        };

        me.okClicked = function(callback){
            onOkClickHandlers.push(callback);
        };
        var registerEvents = function () {
            //On dialog close event
            $(me.window).on('close', function () {
                me.removeWindow();
            });

            //On no button click event
            $(me.window).find('.btn-ok').on('click', function () {
                for(var i = 0; i < onOkClickHandlers.length; i++){
                    var callback = onOkClickHandlers[i];
                    callback(me);
                }
                
                me.closeWindow();
            });
        };

        var build = function () {
            me.window = $(getLayout()).jqxWindow(me.settings.windowSettings);
            registerEvents();
        };

        build();

        return me;
    };
}(jQuery));