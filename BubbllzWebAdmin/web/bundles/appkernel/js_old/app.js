function applyFilters($column) {
    var filterType = $column.attr("data-column");
    var fieldName = $column.attr('data-field');
    var columnTitle = $column.text();
    if (filterType === 'datetime') {
        $column.html($column.html() + '<br/><br/><input type="text" data-name="' + fieldName + '_from" class="datepicker datestart prevent-sort table-filter" placeholder="from" /><br/><br/>\n\
                        <input type="text" data-name="' + fieldName + '_to" class="datepicker datestart prevent-sort table-filter" placeholder="to" />');
    } else if (filterType === "dropdown") {
        var html = '<br/><br/><select class="prevent-sort dropdown table-filter" data-name="' + fieldName + '">';
        var options = $column.attr('data-values').split(":");
        for (var i = 0; i < options.length; i++) {
            var values = options[i].split("_");
            html += '<option value="' + values[0] + '">' + values[1] + '</option>';
        }
        html += '</select>';
        $column.html($column.html() + html);
    } else if (filterType === "text") {
        var html = '<br/><br/><input type="text" data-name="' + fieldName + '" class="prevent-sort table-filter" placeholder="' + columnTitle + '" />';
        $column.html($column.html() + html);
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}