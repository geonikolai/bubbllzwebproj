var isMobileMode = false;
var canScroll = true;
var imageRatio = 0;
var mobileInterval;
var handTimeout1;
var handTimeout2;
var handTimeout3;
var handTimeout4;
var imageHeightt;
var canShowMobile = false;
var canAnimateHand;
var isInitialized = false;
var isFormPosted = false;

$(window).on("load", function () {
    if (!isInitialized) {
        initializeSlider();

        //Firefox
        $('body').bind('DOMMouseScroll', function (e) {
            if (!isMobileMode) {
                $('.upper').hide();
                if (canScroll) {
                    if (e.originalEvent.detail > 0) {
                        //scroll down
                        scrollDown();
                    } else {
                        //scroll up
                        scrollUp();
                    }
                }
                //prevent page fom scrolling
                return false;
            } else {
                showAppropriateMobileElements();
            }
        });

        //IE, Opera, Safari
        $('body').bind('mousewheel', function (e) {
            if (!isMobileMode) {
                $('.upper').hide();
                if (canScroll) {
                    if (e.originalEvent.wheelDelta < 0) {
                        //scroll down
                        scrollDown();
                    } else {
                        //scroll up
                        scrollUp();
                    }
                }
                //prevent page fom scrolling
                return false;
            } else {
                showAppropriateMobileElements();
            }
        });

        if (!isMobileMode) {
            document.addEventListener('touchstart', handleTouchStart, false);
            document.addEventListener('touchmove', handleTouchMove, false);

            var xDown = null;
            var yDown = null;

            function handleTouchStart(evt) {
                xDown = evt.touches[0].clientX;
                yDown = evt.touches[0].clientY;

            }
            ;

            function handleTouchMove(evt) {

                if (!xDown || !yDown) {
                    return;
                }

                var xUp = evt.touches[0].clientX;
                var yUp = evt.touches[0].clientY;

                var xDiff = xDown - xUp;
                var yDiff = yDown - yUp;

                if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
                    if (xDiff > 0) {
                        /* left swipe */
                    } else {
                        /* right swipe */
                    }
                } else {
                    if (yDiff > 0) {
                        /* up swipe */

                        if (canScroll) {
                            scrollDown();
                        }
                    } else {
                        /* down swipe */
                        if (canScroll) {
                            scrollUp();
                        }
                    }
                }
                /* reset values */
                xDown = null;
                yDown = null;
            }
            ;
        } else {
            document.addEventListener('touchstart', handleTouchStart1, false);
            document.addEventListener('touchmove', handleTouchMove1, false);

            var xDown = null;
            var yDown = null;

            function handleTouchStart1(evt) {
                xDown = evt.touches[0].clientX;
                yDown = evt.touches[0].clientY;

            }
            ;

            function handleTouchMove1(evt) {

                if (!xDown || !yDown) {
                    return;
                }

                var xUp = evt.touches[0].clientX;
                var yUp = evt.touches[0].clientY;

                var xDiff = xDown - xUp;
                var yDiff = yDown - yUp;

                if (Math.abs(xDiff) > Math.abs(yDiff)) {
                } else {
                    if (yDiff > 0) {
                        if (canScroll) {
                            showAppropriateMobileElements();
                        } else {
                            showAppropriateMobileElements();
                        }
                    }
                }

                xDown = null;
                yDown = null;
            }
            ;
        }

        $('.check').click(function () {
            if (!isMobileMode) {
                var currentIndex = parseInt($('.checked').attr('data-slide-num'));
                var newIndex = parseInt($(this).attr('data-slide-num'));
                for (var i = 0; i < Math.abs(newIndex - currentIndex); i++) {
                    if (currentIndex > newIndex) {
                        scrollUp();
                    } else {
                        scrollDown();
                    }
                }
            }
            return false;
        });

        $('form > div').click(function () {
            $(this).find('input').slideDown(500, function () {
                //fixFormSize();
            });
            $(this).find('input').focus();
            $(this).css("padding", 0);
            $(this).css("margin", 0);
        });

        $('form input[type=text]').focus(function () {
            var $this = $(this);
            $this.parent().find('label').focus();
            showHideLabels($this);
        });

        imageHeightt = getImageHeight();

        $('form').submit(function () {
            if ($(this).find('#bubbllz_entitiesbundle_talkclient_fullName').val() === "" || $(this).find('#bubbllz_entitiesbundle_talkclient_fullName').val() === null || $(this).find('#bubbllz_entitiesbundle_talkclient_fullName').val() === undefined) {
                alert('Το ονοματεπώνυμο δεν μπορεί να είναι κενό!');

                return false;
            }

            if ($(this).find('#bubbllz_entitiesbundle_talkclient_contactPhone').val() === "" || $(this).find('#bubbllz_entitiesbundle_talkclient_contactPhone').val() === null || $(this).find('#bubbllz_entitiesbundle_talkclient_contactPhone').val() === undefined) {
                alert('Το email δεν μπορεί να είναι κενό!');

                return false;
            }
        });

        var currentIndex = parseInt($('#data-index').attr('data-index'));
        if (currentIndex === 2 || currentIndex === 3 || currentIndex === 4 || currentIndex === 5)
        {
            $('.check' + currentIndex).click();
        }
        isInitialized = true;
    }
});

$(document).ready(function () {

});

function showAppropriateMobileElements() {
    if ($('.not-visible2').isOnScreen() === true) {
        $('.not-visible2').animate({
            opacity: 1
        }, 1500);

        $('.not-visible22').animate({
            opacity: 1
        }, 1500);
    }

    if ($('.not-visible3').isOnScreen() === true) {
        $('.not-visible3').animate({
            opacity: 1
        }, 1500);

        $('.not-visible33').animate({
            opacity: 1
        }, 1500);
    }

    if ($('.not-visible4').isOnScreen() === true) {
        $('.not-visible4').animate({
            opacity: 1
        }, 1500);

        $('.not-visible44').animate({
            opacity: 1
        }, 1500);
    }

    if ($('.not-visible5').isOnScreen() === true) {
        $('.not-visible5').animate({
            opacity: 1
        }, 1500);

        $('.not-visible55').animate({
            opacity: 1
        }, 1000);
    }
}

$.fn.isOnScreen = function () {

    var win = $(window);

    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};

window.onresize = function (event) {
    if (!isMobileMode) {
        initializeSlider();
        imageHeightt = getImageHeight();
    }
};

function showHideLabels($el) {
    $('form > div').each(function () {
        var $this = $(this);
        if ($this.find('input').attr('name') !== $el.attr('name')) {
            if ($this.find('input').val() !== "") {
                $this.find('label').hide();
            } else {
                $this.find('label').show();
                $this.find('input').hide();
                $this.css("padding-bottom", 8);
                $this.css("margin-top", 8);
                if ($(window).width() < 361 && $(window).height() < 641) {

                }
            }
        }
    });

    //$('.mobile-background-form').height($('.mobile-image-form form').height() + 40);
}

function initializeSlider() {
    if ($(window).width() < $(window).height() || $(window).width() < 981) {
        isMobileMode = true;
    }
    showSpinner();
    if (!isMobileMode) {
        centerMainContent();
        sliderImageHorizontalPosition();
        sliderTextHorizontalWidth();
        sliderTextVerticalPosition();
        radioVerticalPosiont();
        initializeDivHeights();
        fixContainerHeight();
        fixMobileImageSize();
        fixMobileImagePosition();
        fixHandSize();
        initHand();
        fixFormSize();
        fixFormPosition();
        removeSpinner();

        sliderTextVerticalPositionAnimated();
        radioVerticalPosiontAnimated();
    } else {
        $('.slideImage ').show();
        $('.mobile-background').height($('.mobile-background').first().height());
        $('.mobile-background-form').height($('.mobile-background').first().height() + 200);
        $('.mobile-background-image').css('margin-top', (($('.mobile-image-mobile-container').height() - $('.mobile-background-image').height()) / 2));
        $('.mobile-background-image').css('left', ($('.mobile-image-mobile-container').width() - $('.mobile-background-image').width()) / 2);
        $('.mobile-image-form form').width($('.mobile-image-mobile-container').width() - 30);
        if ($('.mobile-background-form').height() < $('.mobile-image-form form').height() + 200) {
            $('.mobile-background-form').height($('.mobile-image-form form').height() + 200);

        }

        if ($(window).height() < 733 && $(window).width() < 413) {

            $('.mobile-image-form').height($('.mobile-image-form form').height() + 200);
            $('.mobile-background-form').height($('.mobile-image-form form').height());
            $('.mobile-image-form form input[type=submit]').css('left', ($('.mobile-image-form form').width() - 20 - $('.mobile-image-form form input[type=submit]').width()) / 2);
//            $('.mobile-image-form form input[type=submit]').css('z-index', 9999);
        }

        $('.mobile-background-hand-container > div').height(300);
        if ($('.mobile-background-hand-container > div').height() < $('.mobile-background').first().height()) {
            $('.mobile-background-hand-container > div').height($('.mobile-background').first().height());
        }

        if ($('.mobile-background-hand').width() > $(window).width()) {
            $('.mobile-background-hand').height($(window).width() - 50);
        }

        if ($('.mobile-background-hand').height() < $('.mobile-background').first().height()) {
            $('.mobile-background-hand').height($('.mobile-background').first().height());
        }
        fixFormSize();
        fixFormPosition();

        fixMobileImagePosition();

        startMobile();
        $("html, body").animate({scrollTop: 0}, 1000);

        removeSpinner();

        showAppropriateMobileElements();
    }
    if (!isInitialized) {
        if (!isMobileMode) {
            $('#letsTalk').click(function () {
                if (!isMobileMode) {
                    var currentIndex = parseInt($('.checked').attr('data-slide-num'));
                    var newIndex = 5;
                    for (var i = 0; i < Math.abs(newIndex - currentIndex); i++) {
                        if (currentIndex > newIndex) {
                            scrollUp();
                        } else {
                            scrollDown();
                        }
                    }
                    return false;
                } else {
                    $("html, body").animate({scrollTop: $(document).height()}, 1000);
                    return false;
                }
            });
        } else {
            $('.letsTalk1').click(function () {
                $("html, body").animate({scrollTop: $(document).height()}, 1000);
                return false;
            });
        }
    }

}

function scrollDown() {
    if (!isMobileMode) {
        stopMobile();
        hideHand();
        canScroll = false;
        var $slide = getNextSlide();
        if ($slide === false) {
            canScroll = true;
            return false;
        }
        renderSlide($slide);
    }
}

function scrollUp() {
    if (!isMobileMode) {
        stopMobile();
        hideHand();
        canScroll = false;
        var $slide = getPreviousSlide();
        if ($slide === false) {
            canScroll = true;
            return false;
        }
        renderSlide($slide);
    }
}

function checkRightCheck() {
    $('.checked').removeClass('checked');
    $('.check' + parseInt($('.active').attr('data-order'))).addClass('checked');
}

function renderSlide($slide) {
    if (!isMobileMode) {
        animateImage($slide);
        animateTextUp($slide);
        checkRightCheck();

        sliderTextVerticalPositionAnimated();
        radioVerticalPosiontAnimated();
    }
}

//function renderSuccessSlide() {
//    if (!isMobileMode) {
//        $('form').hide();
//        $('.form-success').show();
//        $('.leftContent').hide();
//        $('.image5 .background').css('position', 'absolute');
//        $('.image5 .background').width($('.image5 .background').width() + 150);
//        $('.image5 .background').css('left', ($('.mainContainer').width() - $('.image5 .background').width()) / 2);
//        $('.form-success').css('padding-top', 100);
//        $('.form-success').css('left', 0 + parseInt($('.image5 .background').css('left')));
//        $('.form-success').css('width', $('.image5 .background').width());
//    }
//}

function animateTextUp($slide) {
    $slide.show();
    var width = $slide.width();
    var sliderTextHeight = $slide.height() + 20;

    $slide.css('width', width);

    $('.active').slideUp(1000, function () {
        canScroll = true;
    });

    $('.active').removeClass('active');
    $slide.addClass('active');

    $('.sliderTexts').height(sliderTextHeight);


}

function animateImage($slide) {
    var $image = $('.' + $slide.attr('data-image'));
    var $currentImage = $('.' + $('.active').attr('data-image'));
    $('.slideImage').css('z-index', 100);
    $image.css('z-index', 101);
    $currentImage.css('z-index', 100);

    $('.active-image').removeClass('active-image');
    $image.addClass('active-image');
    if (parseInt($slide.attr('data-order')) > parseInt($('.active').attr('data-order'))) {
        $currentImage.slideUp({
            duration: 1000,
            easing: 'easeOutQuad',
            complete: slideChanged
        });
    } else {
        $image.slideDown({
            duration: 1000,
            easing: 'easeOutQuad',
            complete: slideChanged
        });
        //$image.slideDown(1000, slideChanged);
    }

}

function slideChanged() {
    if ($('.active').attr('data-order') === "3") {
        startMobile();
    } else {
        stopMobile();
    }

    if ($('.active').attr('data-order') === "4") {
        showHand();
    } else {
        hideHand();
    }
}

function fixFormPosition() {
    if (!isMobileMode) {

    } else {
        $('form').css('left', ($('.mobile-image-form').width() - $('.mobile-image-form form').width()) / 2);
        $('form').css('top', ($('.mobile-background-form').height() - $('.mobile-image-form form').height()) / 2);

//        if ($(window).height() < 641 && $(window).width() < 361) {
//            $('form').css('top', 10);
//            $('.mobile-image-form').css('margin-top', 20);
//        }
    }
}

function fixFormSize() {
    if (!isMobileMode) {
//        var top = ($('.mainContainer').height() - $('form').height()) / 2;
//        $('form').animate({
//            top: top
//        }, 500);
        $('form').css('top', ($('.mainContainer').height() - $('form').height()) / 2);
        $('form').width($('.' + $('.active').attr('data-image')).width() - 400);
    } else {
        $('.mobile-image-form form').width($('.mobile-image-form').width() - 100);
        $('.mobile-background-form').height($('.mobile-background-form').height() + 90);

        if ($(window).width() < 413 && $(window).height() < 733) {
            $('.mobile-image form').width($('.mobile-background-form').width() - 40);
        }
    }
}

function initHand() {
    $('.slide4-images').hide();
    $('.slide4-images1').show();
    $('.slide4-images1').css('right', -$(window).width());
}

function showHand() {
    initHand();
    canAnimateHand = true;
    $('.slide4-images1').show();
    $('.slide4-images1').animate({
        right: '0'
    }, 1000, function () {
        if (canAnimateHand) {
            $('.slide4-images2').show();
            $('.slide4-images1').hide();
            handTimeout1 = setTimeout(function () {
                if (canAnimateHand) {
                    $('.slide4-images3').show();
                    $('.slide4-images2').hide();
                    handTimeout2 = setTimeout(function () {
                        if (canAnimateHand) {
                            $('.slide4-images4').show();
                            $('.slide4-images3').hide();
                        }

                    }, 1000);
                }
            }, 1000);
        }

    });
}

function hideHand() {
    canAnimateHand = false
    clearTimeout(handTimeout1);
    clearTimeout(handTimeout2);
    clearTimeout(handTimeout3);
    clearTimeout(handTimeout4);

    $('.slide4-images').hide();
}

function startMobile() {
    if (isMobileMode) {
        $('.mobile-background-hand').hide();
        $('.mobile-background-image').hide();
    }
    $('.mobileImage1').show();
    $('.mobileImage1').addClass('active-mobile');
    $('.mobile-background-image1').show();
    $('.mobile-background-image1').addClass('active-mobile');
    canShowMobile = true;
    mobileInterval = setInterval(function () {
        if (canShowMobile) {
            var nextId = parseInt($('.active-mobile').attr('data-id'));

            $('.active-mobile').fadeOut(500);
            $('.active-mobile').removeClass('active-mobile');
            switch (nextId) {
                case 1:
                case 2:
                case 3:
                    nextId++;
                    break;
                case 4:
                    nextId = 1;
                    break;
            }
            $('.mobileImage' + nextId).show();
            $('.mobileImage' + nextId).addClass('active-mobile');

            $('.mobile-background-hand' + nextId).show();
            $('.mobile-background-hand' + nextId).addClass('active-mobile');

            $('.mobile-background-image' + nextId).show();
            $('.mobile-background-image' + nextId).addClass('active-mobile');
        }
    }, 2000);
}

function stopMobile() {
    canShowMobile = false;
    clearInterval(mobileInterval);
    $('.mobileImage').hide();
}

function getNextSlide() {
    var $next = $('.active').next();
    if ($next.length > 0) {
        return $next;
    }
    return false;
}

function getPreviousSlide() {
    var $next = $('.active').prev();
    if ($next.length > 0) {
        return $next;
    }
    return false;
}

function centerMainContent() {
    if (!isMobileMode) {
        var mainContentWidth = $('.mainContainer').width();
        var windowWidth = $(window).width();
        $('.mainContainer').css('left', (windowWidth - mainContentWidth) / 2);
    }
}

function removeSpinner() {
    $('#spinner').fadeOut(800, function () {
        $('#spinner').hide();
    });
}

function showSpinner() {
    $('#spinner').show();
}

function sliderTextHorizontalWidth() {
    if (!isMobileMode) {
        var leftContentWidth = $('.leftContent').width();
        $('.sliderTexts').width(leftContentWidth + (leftContentWidth / 2));
    }
}

function sliderTextVerticalPosition() {
    if (!isMobileMode) {
        var imageHeight = getImageHeight();

        var textHeight = $('.sliderTexts').first().height();
        var textPadding = parseInt($('.sliderTexts').first().css('padding-bottom')) + parseInt($('.sliderTexts').first().css('padding-top'));
        $('.leftContent').css('margin-top', (imageHeight - textHeight) / 2 - textPadding + 10);
    }
}

function sliderTextVerticalPositionAnimated() {
    if (!isMobileMode) {
        var imageHeight = imageHeightt;

        var textHeight = $('.sliderTexts').first().height();
        var textPadding = parseInt($('.sliderTexts').first().css('padding-bottom')) + parseInt($('.sliderTexts').first().css('padding-top'));

        $('.leftContent').animate({
            'margin-top': (imageHeight - textHeight) / 2 - textPadding + 10
        }, 500);
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function getImageHeight() {
    if (!isMobileMode) {
        var $imageHeight = $('.image1').height();
        var $containerHeight = $('.mainContainer').height();

        if ($imageHeight < 10) {
            $imageHeight = $('.image2').height();
        }

        if ($imageHeight > $containerHeight) {
            $imageHeight = $containerHeight;
        }

        return $imageHeight;
    }
}

function radioVerticalPosiont() {
    if (!isMobileMode) {
//        var slideImageHeight = $('.leftContent').height();
//        var textHeight = $('.radios').first().height();
//        $('.radios').css('margin-top', (slideImageHeight - textHeight) / 2);
    }
}

function radioVerticalPosiontAnimated() {
    if (!isMobileMode) {
        var slideImageHeight = $('.leftContent').height();
        var textHeight = $('.radios').first().height();
        $('.radios').animate({
            'margin-top': (slideImageHeight - textHeight) / 2
        }, 500);
    }
}

function sliderImageHorizontalPosition() {
    if (!isMobileMode) {
        var leftContentWidth = $('.leftContent').width();
        var mainContentWidth = $('.mainContainer').width();
        $('.rightContent').css('left', (leftContentWidth / 2));
        var rigthPadding = 90;
        if ($(window).width() < 1441) {
            rigthPadding = 150;
            $('.sliderTexts').css('padding-left', '70px');
        }
        $('.rightContent').css('width', mainContentWidth - leftContentWidth - rigthPadding);

    }
}

function fixMobileImageSize() {
    var height = getImageHeight() + (getImageHeight() / 1.75);

    if (height > 800) {
        height = 800;
    }

    $('.mobileImage').height(height);
}

function fixMobileImagePosition() {
    if (!isMobileMode) {
        var slideHeight = getImageHeight();
        var mobileHeight = $('.mobileImage').height();
        var result = (mobileHeight - slideHeight) / 2 - 25;
        if (result < 0) {

        }

        var leftWidth = $('.leftContent').width() + $('.radios').width() + $('.leftContent').offset().left;

        $('.mobileImage').css('margin-top', -result);
        $('.mobileImage').css('left', leftWidth + (($('.active-image').width() - ($('.mobileImage').width())) / 2));

        if (($('.sliderTexts').width() + $('.radios').width() + 100) > parseInt($('.mobileImage').css('left'))) {
            $('.mobileImage').css('left', leftWidth + (($('.active-image').width() - ($('.mobileImage').width())) / 2) + 70);
        }
    } else {
        $('.mobile-background-image').css('margin-left', '20px');
    }
}

function initializeDivHeights() {
    if (imageRatio === 0) {
        imageRatio = $('.image1 img').height() / $('.image1 img').width();
    }

    $slideHeight = $('.' + $('.active').attr('data-image') + ' .background').width() * imageRatio;
    $('.slideImage > div').height($slideHeight);
}

function fixContainerHeight() {
    if (imageRatio === 0) {
        imageRatio = $('.image1 img').height() / $('.image1 img').width();
    }

    $slideHeight = $('.' + $('.active').attr('data-image') + ' .background').width() * imageRatio;
    $('.mainContainer').height($slideHeight);
    space = -1 * ($('.mainContainer').offset().top + $('.mainContainer').height() - $(window).height());
    if (space < 90) {
        $('.mainContainer').height(($(window).height() - 90 - $('.mainContainer').offset().top));
    }
}

function fixHandSize() {
    $('.slide4-images').height($('.mainContainer').height() * 1.12);
}